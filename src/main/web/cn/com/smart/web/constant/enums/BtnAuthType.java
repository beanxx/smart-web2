package cn.com.smart.web.constant.enums;

import com.mixsmart.utils.StringUtils;

/**
 * @author 乌草坡 2019-09-05
 * @since 1.0
 */
public enum BtnAuthType {

    ADD("add", "添加"),

    EDIT("edit", "编辑"),

    DELETE("del", "删除"),

    REFRESH("refresh", "刷新"),

    EXPORT("export", "导出"),

    AUDIT("audit", "审批"),

    EDIT_DESIGNER("edit_designer", "修改");

    private String value;

    private String text;

    BtnAuthType(String value, String text) {
        this.value = value;
        this.text = text;
    }

    /**
     * 获取按钮权限类型对象
     * @param value 类型值
     * @return 返回类型对象
     */
    public static BtnAuthType getObj(String value) {
        if(StringUtils.isEmpty(value)) {
            return null;
        }
        BtnAuthType type = null;
        for(BtnAuthType typeTmp : BtnAuthType.values()) {
            if(typeTmp.getValue().equals(value)) {
                type = typeTmp;
                break;
            }
        }
        return type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
