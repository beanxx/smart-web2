package cn.com.smart.report.service.impl;

import cn.com.smart.report.bean.entity.TReportCustomCell;
import cn.com.smart.service.impl.MgrServiceImpl;
import com.mixsmart.utils.CollectionUtils;
import com.mixsmart.utils.LoggerUtils;
import com.mixsmart.utils.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 乌草坡 2019-09-01
 * @since 1.0
 */
@Service
public class ReportCustomCellService extends MgrServiceImpl<TReportCustomCell> {


    /**
     * 更新按钮，操作方式为：先删除后添加
     * @param reportId 报表ID
     * @param customCells 按钮列表
     */
    public void updateCustomCells(String reportId, List<TReportCustomCell> customCells) {
        if(StringUtils.isEmpty(reportId)) {
            LoggerUtils.debug(logger, "报表ID为空");
            return;
        }
        Map<String, Object> param = new HashMap<>(1);
        param.put("reportId", reportId);
        super.deleteByField(param);
        if(CollectionUtils.isEmpty(customCells)) {
            return;
        }
        for(TReportCustomCell customCell : customCells) {
            customCell.setReportId(reportId);
        }
        super.save(customCells);
    }

}
