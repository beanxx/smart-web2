/**
 * 创建表
 */
(function($){
	
	/**
	 * 表单映射配置监听
	 */
	$.fn.formMappingConfigListener = function() {
		var $this = $(this);
		var fieldSeqNum = 1;
		var sourceFields = [];
		var targetFields = [];
		var sourceFormId = null;
		var targetFormId = null;
		init();

		/**
		 * 初始化方法
		 */
		function init() {
			var sourceFormId = $("#source-form").data('default-value');
			var targetFormId = $("#target-form").data('default-value');
			if(utils.isNotEmpty(sourceFormId))
				initSourceField(sourceFormId);
			if(utils.isNotEmpty(targetFormId))
				initTargetField(targetFormId);
		}

		//添加字段映射
		$(".add-mapping-field").click(function() {
			var $tbody = $this.find("#form-mapping-field-table tbody");
			if(!utils.isEmpty($tbody.html())) {
				var $lastTr = $tbody.find("tr:last");
				var lastSeqNum = $lastTr.find(".seq-num").text();
				fieldSeqNum = parseInt(lastSeqNum)+1;
			}
			var trId = 'tr'+fieldSeqNum;
			var fieldElementTr = "<tr id=\""+trId+"\"><td class=\"seq-num text-right\" style=\"width: 40px;\">" +
				"<input type=\"hidden\" class=\"sort-order\" name=\"fieldMappings["+(fieldSeqNum-1)+"].sortOrder\" value=\""+fieldSeqNum+"\" />"+
				"<span class=\"sort-order-label\">"+fieldSeqNum+"</span></td>"+
				"<td><select class=\"form-control source-form-field require\" name=\"fieldMappings["+(fieldSeqNum - 1)+"].fieldId\"><option value=\"\">请选择</option></select></td>"+
				"<td><select class=\"form-control target-form-field require\" name=\"fieldMappings["+(fieldSeqNum - 1)+"].targetFieldId\"><option value=\"\">请选择</option></select></td>"+
				"<td class=\"del-td\" id=\"del-field-mapping"+fieldSeqNum+"\" class=\"text-center\"></td>"+
				"</tr>";
			$tbody.append(fieldElementTr);
			$("<button type=\"button\" title=\"删除\" class=\"delete-field-mapping text-center\" style=\"float: none;font-size: 18px;\" data-dismiss=\"tr1\" aria-label=\"Close\"><i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i></button>").click(function(){
				$(this).parent().parent().remove();
				fieldResort();
			}).appendTo("#del-field-mapping"+fieldSeqNum);
			var $tr = $tbody.find("#"+trId);
			var $sourceSelect = $tr.find('.source-form-field');
			var $targetSelect = $tr.find('.target-form-field');
			utils.selectDataItem($sourceSelect, sourceFields, null);
			utils.selectDataItem($targetSelect, targetFields, null);
			formRequireListener($tr);
		});

		$("#source-form").change(function () {
			console.log('触发change事件');
			var $this = $(this);
			sourceFormId = $this.val();
			initSourceField(sourceFormId);
		});
		$('#target-form').change(function () {
			var $this = $(this);
			targetFormId = $this.val();
			if(sourceFormId == targetFormId) {
				$(".target-form-field .cnoj-dyn-opt").remove();
				utils.showMsg('相同表单不需要设置映射关系');
				return;
			}
			initTargetField(targetFormId);
		});

		/**
		 * 初始化原字段
		 * @param sourceFormId
		 */
		function initSourceField(sourceFormId) {
			getFieldData(sourceFormId, function (response) {
				sourceFields = response.datas;
				fillFieldOption(".source-form-field", sourceFields);
			});
		}

		/**
		 * 初始化目标字段
		 * @param targetFormId
		 */
		function initTargetField(targetFormId) {
			getFieldData(targetFormId, function (response) {
				targetFields = response.datas;
				fillFieldOption(".target-form-field", targetFields);
			});
		}

		/**
		 * 填充字段选项
		 * @param selectTag
		 * @param datas
		 */
		function fillFieldOption(selectTag, datas) {
			if(utils.isEmpty(datas)) {
					$(selectTag+" .cnoj-dyn-opt").remove();
					return;
			}
			$(selectTag).each(function () {
				var $select = $(this);
				var defaultValue = $select.data("default-value");
				utils.selectDataItem($select, datas, defaultValue);
			});
		}

		function getFieldData(formId, callback) {
			$.get('op/query/get_select_form_field_mapping.json', {formId: formId}, function (response) {
				if(typeof(callback) === 'function') {
					callback(response);
				}
			});
		}

		//删除字段
		$(".delete-field-mapping").click(function() {
			$(this).parent().parent().remove();
			fieldResort();
		});

		/**
		 * 重新排序
		 */
		function fieldResort() {
			//重新排序
			var index = 1;
			var $tbody = $this.find("#form-mapping-field-table tbody");
			$tbody.find("tr").each(function(){
				var $tr = $(this);
				$tr.attr("id", "tr"+index);
				$tr.find(".sort-order").val(index);
				$tr.find(".sort-order-label").text(index);
				$tr.find(".del-td").attr("id","del-field-mapping"+index);
				$tr.find("input,select").each(function(){
					var $element = $(this);
					var name = $element.attr("name");
					name = name.replace(/fieldMappings\[\d+\]\./, "fieldMappings["+(index-1)+"].");
					$element.attr("name", name);
				});
				index++;
			});
		}
	}
})(jQuery);