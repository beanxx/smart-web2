package cn.com.smart.form.parser;

import cn.com.smart.form.enums.TextPluginType;
import cn.com.smart.form.helper.ListctrlHelper;
import cn.com.smart.form.list.IFormListFieldParser;
import cn.com.smart.form.list.IListctrlEnableParser;
import cn.com.smart.form.list.bean.AbstractListFieldProp;
import cn.com.smart.form.list.bean.CommonListFieldProp;
import cn.com.smart.form.list.bean.DateListFieldProp;
import cn.com.smart.form.list.bean.TextPluginListFieldProp;
import com.mixsmart.constant.IMixConstant;
import com.mixsmart.enums.YesNoType;
import com.mixsmart.utils.StringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 解析控件列表
 * @author lmq
 * @version 1.0 
 * @since 1.0
 */
@Component
public class ListctrlParser implements IFormParser, IFormListFieldParser, IListctrlEnableParser {

	private static final String UNBIND_FLAG = "unbind";

	@Override
	public String getPlugin() {
		return "listctrl";
	}

	@Override
	public String parse(Map<String, Object> dataMap) {
		if(null == dataMap || dataMap.size()<1) {
			return null;
		}

		ListctrlHelper listctrlHelper = new ListctrlHelper(dataMap);

		String fillUrl = StringUtils.handleNull(dataMap.get("fill_url"));
		String fillRelateField = StringUtils.handleNull(dataMap.get("fill_relate_field"));
		
		String unit = StringUtils.handleNull(dataMap.get("orgunit"));
		String sum = StringUtils.handleNull(dataMap.get("orgsum"));
		//String sumBindTable = StringUtils.handleNull(dataMap.get("csum_bind_table"));
		String sumBindTableField = StringUtils.handleNull(dataMap.get("csum_bind_table_field"));

		String fieldRequire = StringUtils.handleNull(dataMap.get("fieldrequire"));
		String fieldHide = StringUtils.handleNull(dataMap.get("fieldhide"));
		
		String colValue = StringUtils.handleNull(dataMap.get("orgcolvalue"));
		String name = listctrlHelper.getTableId();
		String tableWidth = StringUtils.handleNull(dataMap.get("table_width"));
		String tableHeight = StringUtils.handleNull(dataMap.get("table_height"));
		String remarks = StringUtils.handleNull(dataMap.get("remarks"));

		String validateExpr = StringUtils.handleNull(dataMap.get("validate_expr"));
		String validateExprMsg = StringUtils.handleNull(dataMap.get("validate_expr_msg"));

		String sumRowName = StringUtils.handleNull(dataMap.get("sum_row_name"));
		String bindSumRowField = StringUtils.handleNull(dataMap.get("bind_sum_row_field"));
		String sumRelateField = StringUtils.handleNull(dataMap.get("sum_relate_field"));
		String sumRowExpr = StringUtils.handleNull(dataMap.get("sum_row_expr"));

		if(UNBIND_FLAG.equals(bindSumRowField)) {
			bindSumRowField = bindSumRowField + "-" + StringUtils.uuid();
		}

		String rowSumBindTable = StringUtils.handleNull(dataMap.get("row_sum_bind_table"));
		String rowSumBindTableField = StringUtils.handleNull(dataMap.get("row_sum_bind_table_field"));
		if(UNBIND_FLAG.equals(rowSumBindTableField)) {
			rowSumBindTableField = rowSumBindTableField + "-" + StringUtils.uuid();
		}

		if(StringUtils.isEmpty(tableWidth)) {
			tableWidth = "100%";
		} else {
			tableWidth = tableWidth.replaceAll("px|PX", "");
			if(!StringUtils.isInteger(tableWidth)) {
				tableWidth = "100%";
			} else {
				tableWidth = tableWidth + "px";
			}
		}
		
		String[] titles = listctrlHelper.getColTitleArray();
		String[] colTypes = listctrlHelper.getColTypeArray();
		String[] pluginTypes = listctrlHelper.getPluginTypeArray();
		String[] pluginUris = listctrlHelper.getPluginUriArray();
		String[] colValues = colValue.split("`");
		String[] fieldNames = listctrlHelper.getFieldNameArray();
		String[] fieldRequires = fieldRequire.split("`");
		String[] fieldHides = fieldHide.split("`");
		String[] remarksArray = remarks.split("`");
		String[] readonlyArray = listctrlHelper.getReadonlyArray();
		
		String[] units = unit.split("`");
		String[] sums = sum.split("`");
		//String[] sumBindTables = sumBindTable.split("`");
		String[] sumBindTableFields = sumBindTableField.split("`");
		for(int i=0;i< sumBindTableFields.length;i++) {
			if(UNBIND_FLAG.equals(sumBindTableFields[i])) {
				sumBindTableFields[i] = sumBindTableFields[i] + "-" + StringUtils.uuid();
			}
		}
		String[] validateExprArray = validateExpr.split("`");
		String[] validateExprMsgArray = validateExprMsg.split("`");

        colValues = listctrlHelper.handleProp(colValues, titles);
        fieldRequires = listctrlHelper.handleProp(fieldRequires, titles);
        fieldHides = listctrlHelper.handleProp(fieldHides, titles);
        remarksArray = listctrlHelper.handleProp(remarksArray, titles);

        units = listctrlHelper.handleProp(units, titles);

        validateExprArray = listctrlHelper.handleProp(validateExprArray, titles);
        validateExprMsgArray = listctrlHelper.handleProp(validateExprMsgArray, titles);

		StringBuilder strBuild = new StringBuilder();
		strBuild.append("<script type=\"text/javascript\">\r\n var addRows=1;\r\n function tbAddRow(dname, isInputEvent) {addRows++;\r\n");
		strBuild.append("   var sTbid = dname+\"_table\";\r\n if(typeof(isInputEvent) == 'undefined') isInputEvent = true;\r\n");
		strBuild.append("  var $addTr = $(\"#\"+sTbid+\" .template\") \r\n ");
		strBuild.append("   //连同事件一起复制   \r\n ");
		strBuild.append("   .clone();\r\n");  
		strBuild.append("   //删除模板标记 \r\n   "); 
		strBuild.append("   $addTr.removeClass(\"template\");$addTr.attr(\"id\",\"row\"+addRows);\r\n var $sum = $addTr.find('.sum');if($sum.length>0){$sum.removeClass('sum')} ");
		strBuild.append("   //修改内部元素 \r\n ");
		strBuild.append("   $addTr.find(\".delrow\").removeClass(\"hide\");\r\n");
		//strBuild.append("$addTr.find(\"input[type=hidden]\").remove();");
		strBuild.append("   $addTr.find(\"input,textarea\").each(function(){\r\n");
		strBuild.append(" var id = $(this).attr(\"id\");if(utils.isNotEmpty(id)) { \r\n");
        strBuild.append(" id = id.replace('row-','row'+addRows+'-');var $this = $(this);$this.attr(\"id\",id);$this.val($this.attr('default-value'));\r\n");
        strBuild.append(" $(this).attr(\"name\", $(this).attr(\"original-name\")); \r\n");
		strBuild.append(" $(this).removeClass(function(index, oldClass){var classArray = oldClass.split(' ');var removeClassName = '';");
		strBuild.append("for(var i=0;i<classArray.length;i++) { if(classArray[i].indexOf('-listener')>=0) { removeClassName +=classArray[i]+' '}} return removeClassName});");
		//strBuild.append(" $(this).removeClass('cnoj-auto-complete-relate-listener cnoj-input-tree-listener cnoj-input-select-relate-listener cnoj-input-org-tree-listener");
        //strBuild.append(" cnoj-auto-complete-listener cnoj-input-select-listener cnoj-datetime-listener cnoj-date-listener cnoj-time-listener');");
        strBuild.append(" $(this).parent().find('.glyphicon-calendar').remove();");
        strBuild.append(" \r\n}});\r\n");
        strBuild.append("   //插入表格  \r\n  ");
        strBuild.append("   $addTr.appendTo($(\"#\"+sTbid+' .listctrl-tbody'));if(isInputEvent){inputPluginEvent();countRowListener()};");
        strBuild.append("  if(typeof(formAddRow) !== 'undefined' && !utils.isEmpty(formAddRow) && typeof(formAddRow)==='function'){formAddRow(addRows);}}\r\n ");
        
        strBuild.append("//统计\r\n ");
        strBuild.append("function sumTotal(dname,e) {\r\n ");
		strBuild.append(" var tsum = 0; \r\n");
		strBuild.append(" $('input[name=\"\'+dname+\'\"]').each(function(){\r\n");
		strBuild.append("          var t = parseFloat($(this).val());\r\n");
		strBuild.append("          if(!t) t=0;\r\n"); 
		strBuild.append("          if(t) tsum +=t;\r\n");
		strBuild.append("          $(this).val(t);\r\n");
		strBuild.append("      });\r\n");
        strBuild.append("  $('#\'+dname+\'_total').val(tsum);$('#\'+dname+\'_total').trigger('change'); \r\n}\r\n");
        
        strBuild.append(" /*删除tr*/\r\n");
        strBuild.append("function fnDeleteRow(obj,dname) { \r\n");
        strBuild.append("  var sTbid = dname+\"_table\";\r\n");
        strBuild.append("  var oTable = document.getElementById(sTbid);\r\n");
        strBuild.append("  while(obj.tagName !=\"TR\") {\r\n");
        strBuild.append("     obj = obj.parentNode;\r\n}\r\n");
        strBuild.append("     var id = $(obj).find('.id-value').val();\r\n");
        strBuild.append(" if(utils.isNotEmpty(id)){ var delValue = $(oTable).find('.del-value').val();\r\n");
        strBuild.append(" if(utils.isNotEmpty(delValue)){delValue = delValue+','+id;} else {delValue=id;}\r\n");
        strBuild.append(" $(oTable).find('.del-value').val(delValue);} \r\n");
        strBuild.append("  oTable.deleteRow(obj.rowIndex);\r\n");
        strBuild.append("//删除后重新计算合计\r\n $('.sum').each(function(){var dname = $(this).attr('name');sumTotal(dname, this)});\r\n }");
        
        strBuild.append(" /*监听修改情况tr*/\r\n");
        strBuild.append(" function changeValue(obj){");
        strBuild.append(" var $table = $(obj).parents(\"table:eq(0)\"); var id =$(obj).parents(\"tr:eq(0)\").find(\".id-value\").val(); ");
        strBuild.append(" if(utils.isNotEmpty(id)){ var changeValue = $table.find('.change-value').val(); var isset=false \r\n");
        strBuild.append(" if(utils.isNotEmpty(changeValue)){if(changeValue.indexOf(id) == -1){changeValue = changeValue+','+id;isset=true}} else {changeValue=id;isset=true}\r\n");
        strBuild.append(" if(isset){$table.find('.change-value').val(changeValue); }}\r\n");
        strBuild.append(" };\r\n");
        strBuild.append("</script>");
        
        StringBuilder thBuild = new StringBuilder(),tbBuild = new StringBuilder(),tfTdBuild = new StringBuilder();
        int isNum = 0,tdNum = 0;
        String require = "";
        String tdEndTag = "</td>";
        thBuild.append("<th class=\"hidden\">");
        thBuild.append("<input type=\"hidden\" class=\"del-value\" name=\""+name+"_del\" />");
        thBuild.append("<input type=\"hidden\" class=\"change-value\" name=\""+name+"_change\" />");
        thBuild.append("</th>");
        tbBuild.append("<td class=\"hidden\">");
        tbBuild.append("<input type=\"hidden\" class=\"id-value\" id=\"row-"+name+"-0\" name=\""+name+"_id\" />");
        tbBuild.append("</td>");
        //String[] sumFlag = sums;
        for (int i=0;i<titles.length;i++) {
        	tdNum++;
        	if(!YesNoType.YES.getStrValue().equals(fieldHides[i])) {
        		thBuild.append("<th>"+titles[i]);
        		if(StringUtils.isNotEmpty(remarksArray[i])) {
        			thBuild.append("<p class=\"help-block\" style=\"margin:0;padding:0\">"+StringUtils.handleNull(remarksArray[i])+"</p>");
        		}
        		thBuild.append("</th>");
        	}
			if(YesNoType.YES.getStrValue().equals(fieldRequires[i])) {
				require = " require";
			} else {
				require = "";
			}
			pluginTypes[i] = pluginTypes[i]+require;
        	//判断是否需要验证
			String validateProp = "";
			if(StringUtils.isNotEmpty(validateExprArray[i])) {
				pluginTypes[i] += " cnoj-value-validate";
				validateProp = " data-validate-expr=\""+validateExprArray[i]+"\" data-validate-expr-msg=\""+StringUtils.handleNull(validateExprMsgArray[i])+"\"";
			}
			String readonlyTag = YesNoType.getSupportDefaultObj(readonlyArray[i]).getValue() ? " readonly=\"readonly\"":" ";
			if("text".equals(colTypes[i])) {
			    //判断字段是否设置为隐藏
				if(!YesNoType.YES.getStrValue().equals(fieldHides[i])) {
					if("cnoj-datetime".equals(pluginTypes[i]) || "cnoj-date".equals(pluginTypes[i]) || "cnoj-time".equals(pluginTypes[i])) {
						tbBuild.append("<td><input id=\"row-"+fieldNames[i]+"-"+(i+1)+"\" "+readonlyTag+" onchange=\"changeValue(this)\" class=\"form-control listctrl-input input-medium "+fieldNames[i]+" "+pluginTypes[i]+"\" type=\"text\" data-label-name=\""+titles[i]+"\" name=\""+fieldNames[i]+"\" original-name=\""+fieldNames[i]+"\" default-value=\""+colValues[i]+"\" value=\""+colValues[i]+"\"></td>");
					} else {
						tbBuild.append("<td><input id=\"row-"+fieldNames[i]+"-"+(i+1)+"\" onchange=\"changeValue(this)\" class=\"form-control listctrl-input input-medium "+fieldNames[i]+" "+pluginTypes[i]+"\" "+validateProp+" type=\"text\" data-label-name=\""+titles[i]+"\" data-uri=\""+pluginUris[i]+"\" name=\""+fieldNames[i]+"\" original-name=\""+fieldNames[i]+"\" default-value=\""+colValues[i]+"\"  value=\""+colValues[i]+"\"></td>");
					}
				} else {
					if(tbBuild.toString().endsWith(tdEndTag)) {
						tbBuild.delete(tbBuild.length() - tdEndTag.length(), tbBuild.length());
						tbBuild.append("<input id=\"row-"+fieldNames[i]+"-"+(i+1)+"\" onchange=\"changeValue(this)\" class=\"form-control listctrl-input hidden input-medium "+fieldNames[i]+" "+pluginTypes[i]+"\" "+validateProp+" type=\"text\" data-label-name=\""+titles[i]+"\" data-uri=\""+pluginUris[i]+"\" name=\""+fieldNames[i]+"\" original-name=\""+fieldNames[i]+"\" default-value=\""+colValues[i]+"\" value=\""+colValues[i]+"\"></td>");
					}
				}
			} else if("textarea".equals(colTypes[i])) {
				if(!YesNoType.YES.getStrValue().equals(fieldHides[i])) {
					tbBuild.append("<td><textarea id=\"row-"+fieldNames[i]+"-"+(i+1)+"\" onchange=\"changeValue(this)\" class=\"form-control listctrl-textarea input-medium "+fieldNames[i]+" "+pluginTypes[i]+"\" type=\"text\" data-label-name=\""+titles[i]+"\" data-uri=\""+pluginUris[i]+"\" name=\""+fieldNames[i]+"\" original-name=\""+fieldNames[i]+"\" >"+colValues[i]+"</textarea></td>");
				} else {
					if(tbBuild.toString().endsWith(tdEndTag)) {
						tbBuild.delete(tbBuild.length() - tdEndTag.length(), tbBuild.length());
						tbBuild.append("<textarea id=\"row-"+fieldNames[i]+"-"+(i+1)+"\" "+readonlyTag+" onchange=\"changeValue(this)\" class=\"form-control listctrl-textarea hidden input-medium "+fieldNames[i]+" "+pluginTypes[i]+"\" type=\"text\" data-label-name=\""+titles[i]+"\" data-uri=\""+pluginUris[i]+"\" name=\""+fieldNames[i]+"\" original-name=\""+fieldNames[i]+"\" >"+colValues[i]+"</textarea></td>");
					}
				}
			} else if("int".equals(colTypes[i]) && "1".equals(sums[i])) {
			    tbBuild.append("<td><input id=\"row-"+fieldNames[i]+"-"+(i+1)+"\" "+readonlyTag+" onchange=\"changeValue(this)\" class=\"form-control listctrl-input sum input-medium "+fieldNames[i]+" "+pluginTypes[i]+"\" "+validateProp+" type=\"text\" data-label-name=\""+titles[i]+"\" name=\""+fieldNames[i]+"\" original-name=\""+fieldNames[i]+"\" onblur=\"sumTotal('"+fieldNames[i]+"', this)\" default-value=\""+colValues[i]+"\" value=\""+colValues[i]+"\"> "+getUnit(units, i)+"</td>");
			} else {
				tbBuild.append("<td><input id=\"row-"+fieldNames[i]+"-"+(i+1)+"\" "+readonlyTag+" onchange=\"changeValue(this)\" class=\"form-control listctrl-input input-medium "+fieldNames[i]+" "+pluginTypes[i]+"\" "+validateProp+" type=\"text\" data-label-name=\""+titles[i]+"\" name=\""+fieldNames[i]+"\" original-name=\""+fieldNames[i]+"\" default-value=\""+colValues[i]+"\" value=\""+colValues[i]+"\"> "+getUnit(units, i)+"</td>");
			}
			//tfTdBuild.append("<td></td>");
		}
		//如果有行统计
		if(StringUtils.isNotEmpty(sumRowName)) {
			thBuild.append("<th>"+sumRowName+"</th>");
			String expr = sumRowExpr;
			String[] relateFeilds = sumRelateField.split(IMixConstant.MULTI_VALUE_SPLIT);
			for(int i=0; i<relateFeilds.length; i++) {
				expr = expr.replace("${"+(i+1)+"}", "${"+relateFeilds[i]+"}");
			}
			String classTmp = "";
			String eventProp = "";
			if(StringUtils.isNotEmpty(rowSumBindTableField)) {
				classTmp = "sum";
				eventProp = "onchange=\"sumTotal('"+bindSumRowField+"', this)\"";
			}
			tbBuild.append("<td><input id=\"row-" + bindSumRowField + "-" + (titles.length + 1) + "\" class=\"form-control listctrl-input cnoj-row-count input-medium " +classTmp +" "+ bindSumRowField + "\" type=\"text\" relate-field=\"" + sumRelateField + "\" count-expr=\"" + expr + "\" name=\"" + bindSumRowField + "\" original-name=\"" + bindSumRowField + "\" default-value=\"0\" value=\"0\" "+eventProp+" /></td>");
		}
        if(sums.length > 0) {
            //int len = sums.length;
            for (int i=0;i<titles.length;i++) {
				if(YesNoType.YES.getStrValue().equals(fieldHides[i])) {
					continue;
				}
                if(StringUtils.isNotEmpty(sums[i])) {
                    if(Integer.parseInt(sums[i]) == 1) {
                        isNum = 1;
                        tfTdBuild.append("<td>合计：<input id=\""+fieldNames[i]+"_total\" onchange=\"changeValue(this)\" type=\"text\" style=\"width:80%;\" class=\"form-control sums input-medium "+sumBindTableFields[i]+" \" name=\""+sumBindTableFields[i]+"\" original-name=\""+sumBindTableFields[i]+"\" onblur=\"sumTotal('"+fieldNames[i]+"', this)\" /> "+getUnit(units, i)+"</td>");
                    } else {
                        tfTdBuild.append("<td></td>");
                    }
                }
            }
        }
        //判断行统计是否合计
        if(StringUtils.isNotEmpty(sumRowName) && StringUtils.isNotEmpty(rowSumBindTableField)) {
        	if(sums.length == 0) {
				for (int i=0; i < titles.length; i++) {
					tfTdBuild.append("<td></td>");
				}
			}
        	tfTdBuild.append("<td>合计：<input id=\""+bindSumRowField+"_total\" onchange=\"changeValue(this)\" type=\"text\" style=\"width:80%;\" class=\"form-control row-sums input-medium "+rowSumBindTableField+" \" name=\""+rowSumBindTableField+"\" original-name=\""+rowSumBindTableField+"\" onblur=\"sumTotal('"+bindSumRowField+"', this)\" /></td>");
		}
        String fillListctrlTag = "";
        String fillRelateFieldAttr = StringUtils.isEmpty(fillRelateField) ? "" : ("data-fill-relate-field='" + fillRelateField+"'");
        String fillUrlAttr = "";
        if(StringUtils.isNotEmpty(fillUrl)) {
            fillListctrlTag = "cnoj-auto-fill-listctrl";
            fillUrlAttr = "data-fill-url='"+fillUrl+"'";
        }
        strBuild.append("<table name=\""+ name +"\" id=\""+ name +"_table\" cellspacing=\"0\" "+fillUrlAttr+" "+ fillRelateFieldAttr +" class=\""+ fillListctrlTag +" list-ctrl table table-bordered table-condensed\" style=\"width:"+tableWidth+"\">");
        strBuild.append("<thead><tr style=\"background-color: #f5f5f5;\"><th colspan=\""+(tdNum+1)+"\"><div class=\"col-sm-6 p-l-5 listctrl-title\">"+listctrlHelper.getTitle()+"</div> <div class=\"col-sm-6 p-r-5 text-right\">");
        strBuild.append("<button class=\"btn btn-sm btn-success listctrl-add-row hidden-print \" type=\"button\" onclick=\"tbAddRow('"+name+"')\">添加一行</button>");
        strBuild.append("</div></th></tr><tr><tr>"+thBuild.toString()+"<th><span class=\"hidden-print\">操作</span></th></tr></thead><tbody class=\"listctrl-tbody\">");
		
        strBuild.append("<tr class=\"template\" id='row-1'>"+tbBuild.toString()+"<td><a href=\"javascript:void(0);\" onclick=\"fnDeleteRow(this,'"+name+"')\" class=\"delrow hide hidden-print\">删除</a></td></tr></tbody>");
        
        if(tfTdBuild.length() > 0) {
        	strBuild.append("<tfooter><tr id='row-tfooter-1'>"+tfTdBuild.toString()+"<td></td></tr></tfooter>");
        }
        strBuild.append("</table>");
        String tableWrapStyle = "";
		if(StringUtils.isNotEmpty(tableHeight)) {
			tableHeight = tableHeight.replaceAll("px|PX", "");
			if(StringUtils.isInteger(tableHeight)) {
				tableHeight = tableHeight + "px";
				tableWrapStyle = "style=\"height:"+tableHeight+"\"";
			}
		}
		//strBuild.insert(0, "<div class=\"cnoj-table-wrap table-wrap-limit\" "+tableWrapStyle+">");
		strBuild.insert(0, "<div "+tableWrapStyle+">");
		strBuild.append("</div>");
		return strBuild.toString();
	}

	/**
	 * 获取单位
	 * @param units
	 * @param index
	 * @return
	 */
	private String getUnit(String[] units, int index) {
	    String unit = "";
	    if(units.length <= (index+1)) {
            unit = units[index];
        }
	    return unit;
	}


	@Override
	public boolean isSupportListField(Map<String, Object> dataMap) {
		String listField = StringUtils.handleNull(dataMap.get("list_field"));
		return (StringUtils.isNotEmpty(listField) && listField.indexOf("1") > -1);
	}

	@Override
	public Collection<AbstractListFieldProp> parseListFields(Map<String, Object> dataMap) {
		ListctrlHelper listctrlHelper = new ListctrlHelper(dataMap);
		String[] titles = listctrlHelper.getColTitleArray();
		if(null == titles) {
			return null;
		}
		String[] fieldIds = listctrlHelper.getFieldNameArray();
		String[] pluginTypes = listctrlHelper.getPluginTypeArray();
		String[] pluginUrls = listctrlHelper.getPluginUriArray();
		String[] colTypes = listctrlHelper.getColTypeArray();
		String tableId = listctrlHelper.getTableId();
		String[] fieldSearchArray =  listctrlHelper.getFieldSearchArray();
		String[] fieldSortArray = listctrlHelper.getFieldSortArray();
		List<AbstractListFieldProp> fieldProps = new ArrayList<>();
		for(int i = 0; i< titles.length; i++) {
			AbstractListFieldProp fieldProp = null;
			if("text".equals(colTypes[i])) {
				if(StringUtils.isNotEmpty(pluginTypes[i])) {
					fieldProp = handlePluginTypeProp(pluginTypes[i], pluginUrls[i]);
				} else {
					fieldProp = new CommonListFieldProp();
				}
			}
			if(null == fieldProp) {
				fieldProp = new CommonListFieldProp();
			}
			fieldProp.setFieldId(fieldIds[i]);
			fieldProp.setTitle(titles[i]);
			fieldProp.setPluginType(getPlugin());
			fieldProp.setTableId(tableId);
			fieldProp.setSupportSearch(YesNoType.getSupportDefaultObj(fieldSearchArray[i]).getValue());
			fieldProp.setSupportSort(YesNoType.getSupportDefaultObj(fieldSortArray[i]).getValue());
			fieldProps.add(fieldProp);
		}
		return fieldProps;
	}

	@Override
	public String getListCtrlTitle(Map<String, Object> dataMap) {
		return StringUtils.handleNull(dataMap.get("title"));
	}

	/**
	 * 处理插件类型
	 * @param pluginType
	 * @param pluginUrl
	 * @return
	 */
	private AbstractListFieldProp handlePluginTypeProp(String pluginType, String pluginUrl) {
		TextPluginType pluginTypeObj = TextPluginType.getObj(pluginType);
		if(null != pluginTypeObj) {
			TextPluginListFieldProp pluginListFieldProp = new TextPluginListFieldProp();
			pluginListFieldProp.setUrl(pluginUrl);
			String textPluginType = null;
			switch (pluginTypeObj) {
				case TEXT_PLUGIN_SELECT:
				case TEXT_PLUGIN_SELECT_RELATE:
					textPluginType = TextPluginType.TEXT_PLUGIN_SELECT.getValue();
					break;
				case TEXT_PLUGIN_AUTO_COMPLETE:
				case TEXT_PLUGIN_AUTO_COMPLETE_RELATE:
					textPluginType = TextPluginType.TEXT_PLUGIN_AUTO_COMPLETE.getValue();
					break;
				case TEXT_PLUGIN_TREE:
					textPluginType = pluginTypeObj.getValue();
					break;
				default:
					pluginListFieldProp = null;
					break;
			}
			pluginListFieldProp.setTextPluginType(textPluginType);
			return pluginListFieldProp;
		} else if("cnoj-datetime".equals(pluginType)
				|| "cnoj-date".equals(pluginType)
				|| "cnoj-time".equals(pluginType)) {
			DateListFieldProp dateListFieldProp = new DateListFieldProp();
			dateListFieldProp.setDateType(pluginType);
			String dateFormat = null;
			switch (pluginType) {
				case "cnoj-datetime":
					dateFormat = "yyyy-mm-dd hh:ii:ss";
					break;
				case "cnoj-date":
					dateFormat = "yyyy-mm-dd";
					break;
				case "cnoj-time":
					dateFormat = "hh:ii:ss";
					break;
					default:
						break;
			}
			dateListFieldProp.setDateFormat(dateFormat);
			return dateListFieldProp;
		}
		return null;
	}
}
