package cn.com.smart.form.list;

/**
 * 表单列表常量定义
 * @author lmq
 *
 */
public class FormListConstant {

    /**
     * 定义字段属性KEY
     */
    public static final String FIELD_PROP_KEY = "listFieldProps";
    
}
